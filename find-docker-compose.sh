#!/bin/bash

if command -v docker-compose >/dev/null; then
  echo docker-compose
elif docker compose >/dev/null; then
  echo docker compose
else
  echo "No docker-compose detected."
  exit 1
fi

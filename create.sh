#!/bin/bash

source "$(dirname "${BASH_SOURCE[0]}")/_config.sh"
mkdir -p ${LOCAL_CLUSTER_DIR}/output/certs || true

set -euxo pipefail

"${LOCAL_CLUSTER_DIR}/prerequisites.sh"

STEP_CLI=$($LOCAL_CLUSTER_DIR/find-step-cli.sh)
if [ ! "$STEP_CLI" == "step" ]; then
  step() {
    "$STEP_CLI" "$@"
  }
fi

set +u
if ! [ -z "$TOOLING_IMAGE_TAG" ] &&
  ! docker pull registry.gitlab.com/with-humans/devops-workshop/flux-cluster/tooling:${TOOLING_IMAGE_TAG}; then
  echo "No image for tooling tag $TOOLING_IMAGE_TAG, using latest"
  TOOLING_IMAGE_TAG=latest
fi
set -u

# ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" pull
# make sure registries are up
if [[ -n "$VAGRANT_USE_HOST_REGISTRIES" ]]; then
  ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/local-pullthrough-registries.vagrant.docker-compose.yaml" up -d
  if [ "$USE_TOOLING_IN_DOCKER" == "yes" ]; then
    ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" up tooling -d
  fi
else
  ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" up -d
fi

# Check ingress certificate
if ! { [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" ] && [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_KEY_FILE}" ] && [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}" ]; } ||
  ! ${STEP_CLI} certificate verify --roots "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"; then
  echo "Generating new ingress certificate and installing the root CA"
  ${LOCAL_CLUSTER_DIR}/generate-ingress-certificates.sh "${DOMAIN}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_KEY_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"
else
  echo "Using existing ingress certificate"
fi

if ! ${STEP_CLI} certificate verify "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"; then
  echo "You will be prompted for your sudo password so step-cli can install the certificate in your CA trust stores"
  ${STEP_CLI} certificate install --all "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}"
fi
${TOOLING} step certificate install --all "$INGRESS_CA_CRT_FILE"

# renovate: datasource=docker depName=rancher/k3s versioning=regex:^v(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)-k3s(?<build>\d+)$
K3S_VERSION=v1.27.2-k3s1

# create cluster
${TOOLING} k3d cluster create \
  --k3s-arg "--disable=traefik@server:*" \
  --image "docker.io/rancher/k3s:${K3S_VERSION}" \
  --agents 2 \
  -p "80:80@loadbalancer" \
  -p "443:443@loadbalancer" \
  --registry-config "$CLUSTER_DIR/local-pullthrough-registries.k3d-registries.yaml" \
  --registry-create registry:0.0.0.0:6003 \
  --api-port 127.0.0.1:42839

# Make sure user owns kube-config
${TOOLING} bash -c 'chown '"$(id -u):$(id -g)"' -R $HOME/.kube/'

${TOOLING} kubectl wait --timeout 5m --for=condition=Available deployment metrics-server -n kube-system

# create DNS rewrite so *.k3d.local.with-humans.org resolves to host.k3d.internal
${TOOLING} kubectl apply -f "${CLUSTER_DIR}/ingress-dns-entry.yaml"
# Restart coredns because reload isn't reliable
${TOOLING} kubectl rollout restart -n kube-system deployment.apps/coredns
${TOOLING} kubectl rollout status -n kube-system deployment.apps/coredns

until ${TOOLING} ${CLUSTER_DIR}/test/test-registries-can-be-accessed-from-k3d-node.sh; do
  sleep 1
done

# install blackbox-exporter
${TOOLING} kubectl create namespace observability
${TOOLING} kubectl create configmap \
  -n observability \
  certificate-host.k3d.internal \
  --from-file "host.k3d.internal.crt=${CLUSTER_DIR}/${INGRESS_CRT_FILE}"

${TOOLING} kubectl create namespace traefik
${TOOLING} kubectl create secret generic tls-default-certificate \
  --namespace traefik \
  --from-file=tls.crt=${INGRESS_CRT_FILE} \
  --from-file=tls.key=${INGRESS_KEY_FILE}

#flux bootstrap git --url=ssh://git@gitlab.com/with-humans/devops-workshop/flux-cluster.git --branch=main --path=cluster --private-key-file output/flux-ssh-key
${TOOLING} kubectl apply -f ${CLUSTER_DIR}/local-cluster/flux-system/gotk-components.yaml
${TOOLING} kubectl apply -f ${CLUSTER_DIR}/local-cluster/flux-system/gotk-sync.yaml

until ${TOOLING} kubectl get deployment traefik -n traefik; do
  sleep 1
done

${TOOLING} kubectl wait --timeout 5m --for=condition=Available deployment traefik -n traefik

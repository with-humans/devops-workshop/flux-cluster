#!/bin/bash

# The name of the step cli depends on the package manager.
if [[ `which step-cli` ]]; then
  echo step-cli
elif [[ `which step` ]]; then
  echo step
else
  echo "No step CLI detected."
  exit 1
fi

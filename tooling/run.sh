#!/bin/bash
set -euxo pipefail

source $(dirname "${BASH_SOURCE[0]}")/../_config.sh

IMAGE_NAME="registry.gitlab.com/with-humans/devops-workshop/flux-cluster/tooling:latest"

docker pull ${IMAGE_NAME}
docker build -t ${IMAGE_NAME} - < "${LOCAL_CLUSTER_DIR}/tooling/Dockerfile"
docker run --rm -it --network=host --uts=host --privileged \
    -v "${LOCAL_CLUSTER_DIR}:/infrastructure" \
    -v "${HOME}/.kube/config:/root/.kube/config" \
    -v "${HOME}/.config/argocd/config:/root/.argocd/config" \
    -v "/var/run/docker.sock:/var/run/docker.sock" \
    ${IMAGE_NAME}
